package exception;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionMain {
    public static void main(String[] args) throws Exception {
        try {

            String a = "test";
            if ("test".equals(a)) {
                throw new FileNotFoundException("TEST");
            }

            if (1!=1) {
                throw new CustomChildException("TEST");
            }

        } catch (CustomChildException e) {
            System.out.println("Enter Custom Child Exception");
        } catch (IOException e) {
            System.out.println("Null");
        } catch (Exception e) {
            System.out.println("Enter Exception");
        }
    }
}
