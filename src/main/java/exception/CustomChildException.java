package exception;

public class CustomChildException extends CustomException {
    public CustomChildException(String message) {
        super(message);
    }
}
