package modifierOutter;

import modifierInner.Store;

public class ModifierAnotherPackage {
    public static void main(String[] args) {
        Store store = new Store();
        System.out.println(store.info);
    }
}
