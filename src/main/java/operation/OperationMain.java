package operation;

public class OperationMain {
    public static void main(String[] args) {

        // Type Concat
        System.out.println("1 + 1 = " + (1 + 1));
        System.out.println("1 + \"1\" = " + (1 + "1"));
        System.out.println("'1' + '1' = " + ('2' + '2'));
        System.out.println(Character.hashCode('2'));


        // Operation Priority
        System.out.println("1 + 2 * 3 / 4 = " + (1 + 2 * 3.0 / 4));
        System.out.println("1 * 3 + 4 * 5 = " + (1 * 3 + 4 * 5));

        // Number Compare
        System.out.println("1 == 1L = " + (1 == 1L));
        Integer firstNumber = 1;
        Long secondNumber = 1L;
        System.out.println("firstNumber.equals(secondNumber) = " + firstNumber.equals(secondNumber));

        // String Compare
        System.out.println("\"test\"==\"test\" = " + ("test"=="test"));
        System.out.println("\"test\".equals(\"test\") = " + ("test".equals("test")));
        String beforeString = new String("test");
        String afterString = new String("test");
        Boolean result = beforeString.equals(afterString);
        System.out.println("new String(\"test\").equals(new String(\"test\")) = " + result);
        System.out.println("new String(\"test\")==new String(\"test\") = " + (beforeString == afterString));
    }
}
