package modifierInner;

public class ModifierSamePackage {
    public static void main(String[] args) {
        Store store = new Store();
        System.out.println(store.info);
        System.out.println(store.address);
        System.out.println(store.branchId);
    }
}
