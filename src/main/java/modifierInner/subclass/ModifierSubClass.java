package modifierInner.subclass;

import modifierInner.Store;

public class ModifierSubClass {
    public static void main(String[] args) {
        Store store = new Store();
        System.out.println(store.info);


        SubStore subStore = new SubStore();
        System.out.println(subStore.getBranchId());
    }
}
