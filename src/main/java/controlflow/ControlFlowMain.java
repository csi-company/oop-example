package controlflow;

public class ControlFlowMain {

    public static void main(String[] args) {
        String testCondition = "";
        if ("123".equals(testCondition)) {
            System.out.println("test condition is 123");
        } else if ("456".equals(testCondition)) {
            System.out.println("test condition is 456");
        } else {
            System.out.println("test condition is not valid");
        }

        for (int i=0; i<10; i++) {
            System.out.println("Before Condition " + i);
            if (i<3) {
                continue;
            }
            System.out.println("After Condition " + i);
        }

        System.out.println("=======================");

        for(int i=0; i<10; i++) {
            System.out.println("Before Condition " + i);
            if (i<3) {
                break;
            }
            System.out.println("After Condition " + i);
        }

        System.out.println("Outter For");
    }
}
