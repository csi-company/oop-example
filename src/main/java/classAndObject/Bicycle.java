package classAndObject;

public class Bicycle {
    // Class Variable
    private static String firmeName;

    // Object Variable
    private Integer miles;
    private Integer wheelNumber;
    private String name;

    public static void info(Bicycle bicycle) {
        System.out.println("miles:" + bicycle.getMiles() +
                ", wheel numbers:" + bicycle.getWheelNumber() +
                ", name:" + bicycle.getName() +
                ", firm name:" + Bicycle.getFirmeName());
    }

    public static String getFirmeName() {
        return firmeName;
    }

    public static void setFirmeName(String firmeName) {
        Bicycle.firmeName = firmeName;
    }

    public Integer getMiles() {
        return miles;
    }

    public void setMiles(Integer miles) {
        this.miles = miles;
    }

    public Integer getWheelNumber() {
        return wheelNumber;
    }

    public void setWheelNumber(Integer wheelNumber) {
        this.wheelNumber = wheelNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
