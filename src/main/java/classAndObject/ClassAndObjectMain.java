package classAndObject;

import java.util.Calendar;

public class ClassAndObjectMain {
    public static void main(String[] args) {
        Bicycle.setFirmeName("First Firm");

        Bicycle bicycle1 = new Bicycle();
        bicycle1.setMiles(20000);
        bicycle1.setName("Funny Bicycle");
        bicycle1.setWheelNumber(2);

        Bicycle.info(bicycle1);

        Bicycle bicycle2 = new Bicycle();
        bicycle2.setMiles(3000);
        bicycle2.setName("Serious Bicycle");
        bicycle2.setWheelNumber(3);

        Bicycle.info(bicycle2);

        Bicycle.setFirmeName("Second Firm");
        bicycle1.setMiles(50000);
        Bicycle.info(bicycle1);
        Bicycle.info(bicycle2);

        StringUtil.concat("test1", "test2");



    }
}
