package overload;

public class Coffee {

    public void blend(String bean) {
        System.out.println("blen with " + bean + " bean.");
    }

    public void blend(Integer coffeeShot) {
        System.out.println("blen with " + coffeeShot + " shot.");
    }

    public void blend(String bean, Integer coffeeShot) {
        System.out.println("blen with " + bean + " bean and " + coffeeShot + " shot.");
    }

    public void blend(String bean, String beanOther) {
        System.out.println("blen with " + bean + " bean and " + beanOther + " bean.");
    }
}
