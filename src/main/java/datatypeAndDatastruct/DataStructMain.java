package datatypeAndDatastruct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class DataStructMain {
    public static void main(String[] args) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("123");
        stringArrayList.add("456");
        stringArrayList.add("123");

        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("A", 123);
        hashMap.put("G", 456);
        hashMap.put("C", 789);
        hashMap.put("B", 789);
        System.out.println(hashMap.get("B"));
        System.out.println(hashMap);

        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("123");
        hashSet.add("456");
        hashSet.add("146");
        System.out.println(hashSet);

        // Array
        int[] intArray = {1,2,3,4};
        long[] longArray = {1L,2L,3L,4L};
    }
}
