package datatypeAndDatastruct;

public class DataTypeMain {
    public static void main(String[] args) {
        // Object
        String stringObj = "123";
        Short shortObj = 123;
        Integer integer = null;
        Long longObj = 200L;
        Float floatObj = 123.123f;
        Double doubleObj = 123.123d;

        // Primitive
        int integerPrimitive = 123;
        long longPrimitive = 123L;
        float floatPrimitive = 123.12f;
        double doublePrimitive = 123.123d;
        boolean booleanPrimitive = true;
    }
}
