package abstractsAndInterface;

public abstract class RoadAbstract {
    private String name;
    private Integer trollway;
    private Integer crossways;

    public abstract String getTotalWay();
    public abstract String getTrollwayName();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTrollway() {
        return trollway;
    }

    public void setTrollway(Integer trollway) {
        this.trollway = trollway;
    }

    public Integer getCrossways() {
        return crossways;
    }

    public void setCrossways(Integer crossways) {
        this.crossways = crossways;
    }
}
