package abstractsAndInterface;

public interface RoadInterface {
    public String getTrollwayName();
    public String getName();
    public String getSurname();
}
