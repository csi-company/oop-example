package polymorphism;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PolymorphismMain {
    public static void main(String[] args) {

        Document graduatedDocument = new GraduatedDocument("Jratip", "Wutthichayan");
        Document transcriptDocument = new TranscriptDocument();
        
        graduatedDocument.setAddress("Bang Khare");

        transcriptDocument.setName("Bank");
        transcriptDocument.setSurname("Bank");
        transcriptDocument.setAddress("Avenue");

        GraduatedDocument graduatedDocument1 = new GraduatedDocument();
        graduatedDocument1.setName("Jiratip");
        graduatedDocument1.setSurname("Wutthichayan");
        graduatedDocument1.setAddress("Bang Khare");
        graduatedDocument1.setAdvisorSignature("Advisor 1");

        TranscriptDocument transcriptDocument1 = new TranscriptDocument();
        transcriptDocument1.setName("Jiratip");
        transcriptDocument1.setSurname("Wutthichayan");
        transcriptDocument1.setAddress("Bang Khare");
        transcriptDocument1.setRegistraterSignature("Registrator 1");

        transcriptDocument1.generatePDF();

        DocumentService documentService = new DocumentService();
        documentService.save(transcriptDocument1);

    }
}
