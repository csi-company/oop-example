package polymorphism;

public class GraduatedDocument extends Document {
    private String advisorSignature;

    public GraduatedDocument() {}

    public GraduatedDocument(String name, String surname) {
        super(name, surname);
    }

    public String getAdvisorSignature() {
        return advisorSignature;
    }

    public void setAdvisorSignature(String advisorSignature) {
        this.advisorSignature = advisorSignature;
    }
}
