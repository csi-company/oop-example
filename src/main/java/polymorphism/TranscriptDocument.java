package polymorphism;

public class TranscriptDocument extends Document {
    private String registraterSignature;

    public String getRegistraterSignature() {
        return registraterSignature;
    }

    public void setRegistraterSignature(String registraterSignature) {
        this.registraterSignature = registraterSignature;
    }

    @Override
    public void generatePDF() {
        super.generatePDF();
        System.out.println("Generate PDF Trascript");
    }
}
