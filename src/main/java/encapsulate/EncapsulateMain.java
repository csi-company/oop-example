package encapsulate;

public class EncapsulateMain {
    public static void main(String[] args) {
        Door door = new Door();
        door.lock();
        System.out.println(door.getLocation());
        door.setLocation("Bang wa");
        System.out.println(door.getLocation());
        System.out.println("Door is " + door.getIsLock());
    }
}
