package encapsulate;

public class Door {
    // Variable
    private Boolean isLock;
    private String location;

    // Method
    public void lock() {
        this.isLock = true;
    }

    public void unLock() {
        this.isLock = false;
    }

    public Boolean getIsLock() {
        return this.isLock;
    }


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
